# Ecommerce Site

## Requirements and Verifications

We are going to deliver an e-commerce site that handles 1,000 requests per
minute.  We will automate the following tasks, and we will use these tasks to
verify our deliverables.

1. We prepare a test stock item and a set of scripts for resetting its stock
   level to a specific state for testing.  E.g. there can be a virtual stock
   level of 10 for item A; then no matter how many items were sold, the scripts
   can always reset the virtual stock level back to 10.
1. By default, the test stock item is hidden unless we go to the e-commerce
   site with a specific token to enable testing (or any equivalent mechanism,
   e.g. whitelisting an IP address).
1. When the e-commerce site is up and running, we go to the site with the test
   token as a customer.
1. We search for the test item.
1. We add 2 test items to the cart.
1. We remove a test item from the cart.
1. We go to the checkout page.
1. We input an invalid delivery address, and we verify the page displays the
   corresponding error messages.
1. We correct the address.
1. We proceed to the payment page.  Depending on the payment method, there are
   several options to verify the payment page is working.  For example, an
   option would be to have a standalone test payment method, and we go through
   the complete flow of the payment process.  Another example would be using
   the specific card numbers or other inputs for testing; e.g. Stripe provides
   a list of card numbers for different errors.  We need to make sure that we
   do not hit any rate limits or brute force protection limits.
1. If we have gone through a complete payment process, we will run the scripts
   for resetting the stock level.

This diagram summarises the flow for requirement verifications.

```
      +---------+
      |         |
      |  Start  |
      |         |
      +----+----+
           |
           v
+----------+----------+
|                     |
| Prepare a test item |
|   and scripts for   |             +-----------------+
|      resetting      |             |                 |
|                     |       +---->+ Buy a test item +-------+
+----------+----------+       |     |                 |       |
           |                  |     +-----------------+       |
           v                  |                               v
    +------+------+   +-------+--------+           +----------+----------+
    |             |   |                |           |                     |
    | Site up and |   | Visit the site |           | Verify the purchase |
    |   running   +-->+ as a customer  |           | and the data        |
    |             |   |                |           |                     |
    +-------------+   +-------+--------+           +----------+----------+
                              ^                               |
                              |       +-------------+         |
                              |       |             |         |
                              +-------+ Reset stock +<--------+
                                      |    level    |
                                      |             |
                                      +-------------+
```

This workflow captures the most common and vital use case.  This can be used as
a test case for smoking testing to verify the site's status.  Running this
workflow after each deployment is particularly useful.  In addition, because
the workflow is repeatable, we can use it to carry out a stress test to verify
the performance of the application.

Here are some sample usage of the workflow.

1. Run after each deployment.
1. Run against a development environment during development.
1. Run dozens of concurrent sessions to verify the performance.

## Cloud Provider

We will be using AWS as the primary cloud provider.  Depending on other
potential requirements, the selection of AWS might not be absolute.  We can
switch to other providers such as Azure and GCP if they offer critical
advantages over AWS.  We can also consider hybrid cloud or on-prem
infrastructure if they can fulfil some particular requirements from third party
regulations and compliance.  Until those requirements become clearer, we will
use AWS as the cloud provider.  Nonetheless the application should be developed
with the concept in mind that, with minimal effort, it can be deployed with a
different vendor in the future.  There are tools and architectures that help
improving portability of the application, such as containerisation, abstract
interfaces and dependency injections.

## Deployment Architecture

These are the key requirements the architecture need to fulfil.

* Able to scale in and out according to the usage.
* Fault tolerance: the system contines to function even after failure of a
  single component.
* Secure.

Similar to the decision of choosing AWS as the provider, the architecture we
described here is not the only option; there are other possible options such as
Elastic Beanstalk or Lambda.

The diagram below summarises the proposed architecture.

```
   +---------------+
   |               |
   | Requests from |
   |   Internet    |
   |               |
   +-------+-------+
           |
           v
    +------+-----+
    |            |
    |   Domain   |
    | Managed by |
    |  Route 53  |
    |            |
    +------+-----+
           |
           | Alias/CNAME
           |
+-------------------------+ VPC Public Subnets +------------------------+
           |
           |         +------------+
           |         |            |
           +-------->+ CloudFront |
                     |            |
                     +------------+
                            |
                            v
                     +------+------+
                     |             +<----------------------+
                     |     ALB     |                       v
                     |  endpoint   |    +---------+   +----+----+
          Managed    |             |    |         |   |         |
          by ACM  +->+ (SSL Cert)  +<-->+ Cognito |   | AWS WAF |
                     |             |    |         |   |         |
                     +-+-----------+    +---------+   +---------+
                       |
                       |
+-------------------------+ VPC Private Subnets +-----------------------+
                       |
                       v
      +----------------+------------------------------+
      |                                               |
      |                  ECS Service                  |
      |                                               |
      | +-----------+ +-----------+     +-----------+ |
      | |           | |           |     |           | |
      | | ECS Task  | | ECS Task  |     | ECS Task  | |
      | |           | |           | ... |           | |
      | | (PHP App) | | (PHP App) |     | (PHP App) | |
      | |           | |           |     |           | |
      | +-----------+ +-----------+     +-----------+ |
      |                                               |
      +-----------------------+-----------------------+
                              |
           +---------------------------------------+
           |                  |                    |
           v                  v                    v
    +------+------+ +---------+----------+ +-------+-------+
    |             | |                    | |               |
    | ElastiCache | | Aurora Serverless  | |      AWS      |
    |             | |                    | | Elasticsearch |
    |   (Redis)   | | (MySQL+compatible) | |               |
    |             | |                    | |               |
    +-------------+ +--------------------+ +---------------+
```

### Scalability

* The PHP application is containerised in Docker.
* The PHP application is configured so that it uses Redis for session storage.
  This is needed so that we can carry out true load balancing without sticky
  sessions.
* The PHP application can run its own frontend.  If we prefer the frontend to
  be a separate SPA frontend, we can host the SPA static assets in a S3 bucket
  served by a CloudFront distribution.  Either way, CloudFront caches the
  static pages and assets to improve performance.
* We enable global edge locations in CloudFront so that we can serve requests
  globally with acceptable latency.  We can further optimise for international
  users by deploying to other regions around the globe and enable geolocation
  routing so that the users are routed to the nearest deployment.
* The PHP application runs in each ECS task.  The minimum number of tasks is
  one in each AZ.  A simple rule for scaling (changing number of tasks) is to
  define an acceptable response time range and scale out when the response time
  is longer than the upper bound and scale in when it is shorter than the lower
  bound.  An example time range could be 150 ms to 600 ms.  The scaling rules
  in production really depend on the actual load; so we must monitor the
  deployment and adjust those rules accordingly.
* All components are deployed across at least 2 availability zones (AZs),
  preferably all of them (3 in Sydney); components include ALB subnet
  connection, ECS tasks, EleatiCache and Elasticsearch.  RDS Serverless is
  multi-AZ by default.
* RDS Serverless automatically scales itself according to the load and number
  of connections.
* For ElastiCache and Elasticsearch, the minimal deployment generally they can
  handle a high volume of access.  The minimum nodes for Elasticsearch is 5 (to
  avoid the split-brain issue).  The minimum nodes for ElastiCache Redis is 2.
  Similar to the application's auto-scaling, we must monitor the performance of
  these components so that the number of nodes can deliver the required load.

### AWS Costs

A preliminary estimation would be $200 to $250 as the monthly base cost, and
$20 to $30 for each hour of peak load (1,000 requests per minute). The monthly
cost for 6-hour daily peak load would be around $3,500 to $5,500 (~ $30 x 6 hrs
x 7 days x 4.333 average weeks). A more detailed AWS cost estimations can be
provided upon request.

### Automation and Security

* All data in-transit and at-rest are encrypted.  We use KMS and we provide our
  key pair.
* WAF provides the first layer of attack monitoring and protection.
* Cognito can be used to facilitate authentication and oauth integration with
  third parties such and Facebook and Google.
* The deployment is completely automated including the CI/CD component itself.
  The developers do not need to have access to the production AWS environment.
* We can use tools like CloudFormation, Terraform and `serverless` for IaC.
* A full history of application configuration is kept secretly.  The developers
  do not need to have access to that information.

## Monitoring

When the deployment is live, we will monitor the performance and any errors
using these tools

* Uptime Robot for basic uptime checking, while ALB is configured to perform
  health check on each member in the target group..
* Sentry for monitor unhandled exceptions from the application.
* Execute the smoking test describe earlier periodically.
* CloudWatch alarms against application response time, error rates, usage, AWS
  costs and etc.
* The application should be scanned periodically for vulnerability using tools
  like OWASP ZAP and SonarScanner.

We recommend dedicating a 24/7 rotated role for responding to any incidents and
alarms to protect revenue generation.

## Preliminary Delivery Timeline

I recommend to use 2-week sprints for this project.  Assuming the functionality
and frontend design are stabilised, for a team of 5 engineers, 2 frontend
engineers, 2 backend engineers and a DevOps Engineer, it is feasible to deliver
the project in 3 months.

The key requirements would be tested at the very early stage of the project.  A
main one is the site's ability to serve 1,000 request per minute, and that can
be verified in a user story at the end of the first sprint as follows.

> Within a minute window, 250 users search for an item on the site; they
> receive the search result within 500 ms; then they add the item to the cart.

Such a user story can be tested using AWS Device Farm.

Here are some other example user stories to be delivered.

1. A shopper visits the site and the homepage of the site is rendered within
   500 ms.
1. A shopper can search for a item.
1. A shopper can add an item to the cart.
1. A shopper can make payment on the checkout page.
1. An engineer will receive an notification when there is an unhandled
   exception.
1. An engineer will receive an notification when the application response time
   is over 1 second.
