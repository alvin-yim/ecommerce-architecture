# Ecommerce Site

## [1.0.0]
### Added
- Test workflow.
- Improve the diagram for the test workflow.
- Cloud provider summary.
- Architecture diagram.
- Deployment notes, monitoring and cost estimations.
- Group the deployment notes into sections.
- Timeline.
